var allPersons = [];
window.addEventListener('load', () => {
  fetch('https://randomuser.me/api/?seed=javascript&results=100&nat=BR&noinfo').then((res) => {
    res.json().then((data) => {
      hiddenLoad();
      let allPersonsFiltered = filterData(data.results);
      allPersons = [...allPersonsFiltered];
      renderUsers();
      console.log(allPersons);
    });
  });
});

const hiddenLoad = () => {
  let load = document.querySelector('#spinner');
  load.classList.add('d-none');
};

const filterData = (data) => {
  const newArray = data.map((item) => {
    return {
      name: `${item.name.first} ${item.name.last} `,
      picture: item.picture,
      age: item.dob.age,
      gender: item.gender,
    };
  });
  return newArray;
};

let inputField = document.querySelector('#inputField');
inputField.addEventListener('keydown', (e) => {
  if (e.key === 'Enter') {
    renderUsers();
  }
});

let button = document.querySelector('#searchButton');
button.addEventListener('click', () => {
  renderUsers();
});

const renderUsers = () => {
  let listUsers = document.querySelector('#listUsers');
  let searchName = inputField.value;
  let names = search(searchName);
  names.sort(function (a, b) {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    return 0;
  });

  renderBarResults(names);
  listUsers.innerHTML = '';
  names.map((item) => {
    let row = renderUserRow(item);
    let li = document.createElement('li');
    li.appendChild(row);
    listUsers.appendChild(li);
  });
  renderStats(names);
};

const renderStats = (names) => {
  let container = document.querySelector('#rowStats');
  container.innerHTML = '';
  if (names.length > 0) {
    let num1 = names.filter((item) => item.gender === 'male').length;
    let num2 = names.filter((item) => item.gender === 'female').length;
    let total = calculateAges(names);

    let numberMen = document.createElement('p');
    let textMen = document.createTextNode(`Sexo Masculino: ${num1}`);
    numberMen.appendChild(textMen);
    let numberWomen = document.createElement('p');
    let textWomen = document.createTextNode(`Sexo Feminino: ${num2}`);
    numberWomen.appendChild(textWomen);
    let totalAges = document.createElement('p');
    let Ages = document.createTextNode(`Soma das idades: ${total}`);
    totalAges.appendChild(Ages);
    let avgAges = document.createElement('p');
    let avg = document.createTextNode(`Média das idades: ${(total / names.length).toFixed(2)}`);
    avgAges.appendChild(avg);

    container.appendChild(numberMen);
    container.appendChild(numberWomen);
    container.appendChild(totalAges);
    container.appendChild(avgAges);
  } else {
    container.appendChild(document.createTextNode('Nada a ser Exibido'));
  }
};

const calculateAges = (names) => {
  const justAges = names.map((item) => item.age);
  const sum = justAges.reduce((total, current) => {
    return total + current;
  }, 0);
  return sum;
};

const renderBarResults = (names) => {
  let topBar = document.querySelector('#barUsers');
  topBar.innerHTML = '';
  let title = '';
  if (names.length > 0) {
    title = document.createTextNode(`${names.length} usuário(s) encontrado(s)`);
  } else {
    title = document.createTextNode(`Nenhum usuário filtrado.`);
  }
  let titleE = document.createElement('p');
  titleE.classList.add('titleBar');
  titleE.appendChild(title);
  topBar.appendChild(titleE);
};

const renderUserRow = (item) => {
  let name = document.createTextNode(`${item.name},`);
  let age = document.createTextNode(` ${item.age} anos`);
  let textRow = document.createElement('p');
  textRow.appendChild(name);
  textRow.appendChild(age);
  textRow.classList.add('name');

  let rowBox = document.createElement('div');
  rowBox.classList.add('rowStyle');
  clickEffect(rowBox, item);

  const picture = renderPicture(item);
  rowBox.appendChild(picture);
  rowBox.appendChild(textRow);

  return rowBox;
};

const search = (searchName) => {
  if (typeof searchName !== 'string' || searchName.length === 0) {
    return [];
  }
  let searchLower = searchName.toLowerCase();
  let filtered = allPersons.filter((person) => {
    if (person.name.toLowerCase().includes(searchLower)) {
      return true;
    }
    return false;
  });
  return filtered;
};

const renderPicture = (item) => {
  let foto = document.createElement('img');
  foto.src = `${item.picture.medium}`;
  foto.classList.add('img');
  return foto;
};

const clickEffect = (element, item) => {
  var modal = document.querySelector('#myModal');
  var span = document.querySelector('.close');

  span.addEventListener('click', () => {
    modal.style.display = 'none';
  });
  window.addEventListener('click', (e) => {
    if (e.target === modal) {
      modal.style.display = 'none';
    }
  });

  element.addEventListener('mousedown', () => {
    element.classList.add('rowStyleOnClick');
  });
  element.addEventListener('mouseup', () => {
    element.classList.remove('rowStyleOnClick');
    renderModal(item);
    modal.style.display = 'block';

    // rowBox.classList.remove('rowStyleOnClick');
  });
};
//nome, idade, genero, foto
const renderModal = (item) => {
  var modalContent = document.querySelector('.modal-content');
  modalContent.innerHTML = '';
  var foto = renderPicture(item);
  modalContent.appendChild(foto);
};
